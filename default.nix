with import <nixpkgs> {};
{
  NoeScrape = stdenv.mkDerivation {
    name = "NoeScrape";
    buildInputs = [
      pythonFull
      python27Packages.lxml
      python27Packages.bpython
      python27Packages.requests
      python27Packages.virtualenv
      python27Packages.suds-jurko

      sqlitebrowser

      zsh
      tmate
      
      nano
      emacs
    ];

  # libxml2, expat, cairo, gtk2
  LD_LIBRARY_PATH="${libxml2.out}/lib:${expat.out}/lib";
  LC_CTYPE="en_US.UTF-8";
  LANG="en_US.UTF-8";
  LOCALE_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";
  
  shellHook = ''
        unset http_proxy
        export SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt
        export GIT_SSL_CAINFO=/etc/ssl/certs/ca-certificates.crt
        export JULIA_PKGDIR=$(realpath ./.julia_pkgs)
#        tmate -L $name new-session ${zsh}/bin/zsh
#        exit
    '';
  };
}
