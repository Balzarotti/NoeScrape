#! /usr/bin/env python2
import requests
from paper import Paper
from lxml import etree, html
import HTMLParser

class Scopus():
    def __init__(self, api_key = '7d65f4812bb92478f6847ac52fbd6b82', count = 25,
                 view="META" # META_ABS, FULL
                 , DEBUG = False
    ):
        self.api_key = api_key
        self.last_query = ""
        self.count = count
        self.view = view
        self.DEBUG = DEBUG
        self.abstract_url = 'http://api.elsevier.com/content/abstract/scopus_id/'
        self.headers = {'Accept':'application/json', 'X-ELS-APIKey':self.api_key}

    def warnNoId(self, paper, what):
        print "[WARN] Article title: %s" % (paper.title)
        print "[WARN] Scopus id not found, not downloading %s" % (what)

    def search(self, query, want_abstract = False, want_metrics = False):
        self.last_query = query

        first = True
        missing = False
        run = 0

        paper_list = []

        while first or missing:
            first = False
            run +=1
            print "[INFO] Getting %s results (run number %s)" % (self.count, run)

            res = requests.get("http://api.elsevier.com/content/search/scopus",
                               headers=self.headers,
                               params={"query":self.last_query,
                                       "count": self.count,
                                       # "date": "start-stop",
                                       # field breaks subtypeDescription
                                       # "field": "dc:title,prims:doi,prism:publicationName,dc:creator,prism:issn,subtypeDescription,prism:volume,prism:coverDate,dc:identifier,prism:pageRange,prism:issue",
                                       "start": self.count*(run-1)}
            ).json()["search-results"]["entry"]

            if "error" in res[0]:
                print "[ERROR] results empty"
                
                break
            
            for p in res:
                newpaper = Paper()
                newpaper.search_on="Scopus"
                newpaper.title    = p.get("dc:title")
                newpaper.journal  = p.get("prism:publicationName")
                newpaper.doi      = p.get("prism:doi")
                # newpaper.author   = p.get("dc:creator") #dc:creator
                newpaper.issn     = p.get("prism:issn")
                newpaper.papertype= p.get("subtypeDescription")
                # newpaper.citedby_count   = p.get("citedby-count")
                newpaper.volume   = p.get("prism:volume")
                newpaper.date     = p.get("prism:coverDate")
                tmpid = p.get("dc:identifier")
                if tmpid:
                    scopus_id = tmpid.replace("SCOPUS_ID:","")
                else:
                    scopus_id = False

                if want_abstract:
                    if scopus_id:
                        print "[INFO] Getting abstract (a bit slow)"
                        abs_req = requests.get("%s%s" % (self.abstract_url, scopus_id),
                                               # params={"field":"link,authors,issueIdentifier"},
                                               params = {
                                                   "view": self.view,
                                               },
                                               headers=self.headers)
                        abs_ret = abs_req.json()["abstracts-retrieval-response"]
                        if self.view == "FULL":
                            newpaper.abstract = abs_ret["abstracts"]
                            newpaper.authors = [auth["author"]["ce:indexed-name"] for auth in abs_ret["authors"]]
                        else:
                            abs_link = abs_ret["coredata"]["link"][1]["@href"]
                            abs_link = HTMLParser.HTMLParser().unescape(abs_link)
                            newpaper.abstract = html.fromstring(requests.get(abs_link).content).xpath('//p[@id="recordAbs"]/text()')[0]
                    else:
                        self.warnNoId(newpaper, "abstract")

                if want_metrics:
                    if scopus_id and newpaper.issn:
                        try:
                            print "[INFO] Getting metrics"
                            metrics = requests.get("http://api.elsevier.com/content/serial/title/issn/%s" % (newpaper.issn),
                                                   headers=self.headers
                            ).json()["serial-metadata-response"]["entry"][0]
                            metricsSJR = metrics["SJRList"]["SJR"]
                            for metric in metricsSJR:
                                if int(metric["@year"]) == 2015:
                                    newpaper.SJR = metric["$"]

                            newpaper.citeScore = metrics["citeScoreYearInfoList"]["citeScoreCurrentMetric"]
                        except KeyError:
                            print "[WARN] SJR not found"
                    else:
                        self.warnNoId(newpaper, "metrics")
                        
                    
                pagespan = p.get("prism:pageRange")

                if pagespan:
                    pagespan = pagespan.split("-")
                    if len(pagespan) != 2:
                        pagespan=[pagespan[0],0]
                else:
                    print "[WARN] Missing pagespan"
                    pagespan = [0,0]

                newpaper.pagespan = (pagespan[0], pagespan[1])

                newpaper.scopus_id = scopus_id
                newpaper.issue    = p.get("prism:issue")
                paper_list.append(newpaper)

            if len(res) == self.count:
                missing = True
            else:
                missing = False

        return paper_list
