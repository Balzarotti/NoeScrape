#! /usr/bin/env python2

from scopus import Scopus
# Add to db
import sqlite3
import os.path

S = Scopus()

dbname = 'papers.db'

to_create = not os.path.isfile(dbname) 

conn = sqlite3.connect(dbname)
c = conn.cursor()

if to_create:
    print "[INFO] Database not found. Creating it"
    # Create table
    c.execute('''CREATE TABLE papers
    (title text, journal text, doi text, wosid text, issn text, scopus_id text,
    pagespan_start int, pagespan_end int, volume text, issue text, date text, abstract text, keywords text, authors text, search_on text, papertype text, SJR text, citeScore text)
''')
    print "[INFO] Database created (%s)" % (dbname)

QUERY = "\"rape joke\""
print "[INFO] Searching for %s on Scopus" % (QUERY)
results = S.search(QUERY, want_abstract = True, want_metrics = False)

print "[INFO] We have downloaded %s papers!" % (len(results))

for paper in results:
    # Insert a row of data
    c.execute("INSERT INTO papers VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
              (paper.title, paper.journal, paper.doi, paper.wosid, paper.issn, paper.scopus_id, paper.pagespan[0], paper.pagespan[1], paper.volume, paper.issue, paper.date, paper.abstract, ", ".join(paper.keywords), paper.author, paper.search_on, paper.papertype, paper.SJR, paper.citeScore))

# Save (commit) the changes
conn.commit()

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
conn.close()


