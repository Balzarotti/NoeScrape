# TODO: find the doi, then search here:
# http://ieeexplore.ieee.org/gateway/ipsSearch.jsp?doi=

import requests
from lxml import html, etree
import urlparse
from paper import Paper
from collections import OrderedDict

class WOS():
    def __init__(self,
                 prID = '704c00e2-b7a6-4532-b9b5-cf1af65bf3af',
                 SID = "",
                 product = "WOS",
                 search_mode = "GeneralSearch",
                 pageSize = 10,
                 page_number = 1,
                 debug = False
    ):
        self.sess = requests.Session()
        self.search_url = "http://apps.webofknowledge.com/Search.do"
        self.search_post_url = "http://apps.webofknowledge.com/WOS_GeneralSearch.do"
        self.paper_url = "http://apps.webofknowledge.com/full_record.do"

        self.search_get = {
            "product":product,
            "search_mode":search_mode,
            "SID":SID,
            "preferencesSaved":""
        }

        self.search_post = OrderedDict([("fieldCount","1"), 
                                        ("action","search"),
                                        ("product","WOS"),
                                        ("search_mode","GeneralSearch"),
                                        ("SID",SID),
                                        ("max_field_count","25"),
                                        ("sa_params","WOS||%s|http://apps.webofknowledge.com|'" % SID),
                                        ("formUpdated","true"),
                                        ("value(input1)",""),
                                        ("value(select1)","TS"),
                                        ("x","58"),
                                        ("y","27"),
                                        ("value(hidInput1)",""),
                                        ("limitStatus","expanded"),
                                        ("ss_lemmatization","On"),
                                        ("ss_spellchecking","Suggest"),
                                        ("SinceLastVisit_UTC",""),
                                        ("SinceLastVisit_DATE",""),
                                        ("period","Range Selection"),
                                        ("range","ALL"),
                                        ("startYear","1970"),
                                        ("endYear","2016"),
                                        ("editions","SCI"),
                                        ("editions","SSCI"),
                                        ("editions","AHCI"),
                                        ("editions","ISTP"),
                                        ("editions","ISSHP"),
                                        ("editions","ESCI"),
                                        ("update_back2search_link_param","yes"),
                                        ("ssStatus","display:none"),
                                        ("ss_showsuggestions","ON"),
                                        ("ss_numDefaultGeneralSearchFields","1"),
                                        ("ss_query_language",""),
                                        ("rs_sort_by","PY.D;LD.D;SO.A;VL.D;PG.A;AU.A")])

        self.save_config_url = 'http://apps.webofknowledge.com/AutoSave_UA_output.do'
        self.save_config_params = {'action':'saveForm',
                                   'SID':SID,
                                   'product':'UA',
                                   'search_mode':'output'}

        self.save_config_post = {'value(saveToMenuDefault)':'other',
                            'product':'UA',
                            'search_mode':'output'}

        self.save_bib_url = 'http://apps.webofknowledge.com/OutboundService.do'
        self.save_bib_params = {'action':'go',
                                       'marked_list_candidates':2,
                                       'excludeEventConfig':'ExcludeIfFromFullRecPage'}
        self.save_bib_post = {
            'IncitesEntitled':'yes',
            'SID':SID,
            'colName':'WOS',
            'count_new_items_marked ':'0',
            'displayCitedRefs':'true',
            'displayTimesCited':'true',
            'displayUsageInfo':'true',
            'fields_selection':'PMID USAGEIND AUTHORSIDENTIFIERS ACCESSION_NUM FUNDING SUBJECT_CATEGORY JCR_CATEGORY LANG IDS PAGEC SABBR CITREFC ISSN PUBINFO KEYWORDS CITTIMES ADDRS CONFERENCE_SPONSORS DOCTYPE ABSTRACT CONFERENCE_INFO SOURC TITLE AUTHORS',
            'filters':'PMID USAGEIND AUTHORSIDENTIFIERS ACCESSION_NUM FUNDING SUBJECT_CATEGORY JCR_CATEGORY LANG IDS PAGEC SABBR CITREFC ISSN PUBINFO KEYWORDS CITTIMES ADDRS CONFERENCE_SPONSORS DOCTYPE ABSTRACT CONFERENCE_INFO SOURC TITLE AUTHORS',
            'format':'saveToFile',
            'locale':'en_US',
            'mark_from':1, # change during the run
            'mark_id':'WOS',
            'mark_to':1, # change during the run
            'mode   ':'OpenOutputService',
            'product':'WOS',
            'qid':1, # change during the run
            'queryNatural':"",
            'recordID':'WOS:000377797400009',
            'rurl':"",# change during the run
            'save_options':'bibtex',
            'search_mode':'GeneralSearch',
            'sortBy':'PY.D;LD.D;SO.A;VL.D;PG.A;AU.A',
            'use_two_ets':'false',
            'viewType':'fullRecord',
            'view_name':'WOS-fullRecord'}
        
        # self.search_params = { "prID":prID,
        #                        "SID":SID,
        #                        "product":product,
        #                        "search_mode":search_mode,
        #                        "pageSize":pageSize,
        #                        "page":page_number}
        self.paper_params = {
            "SID":SID,
            "doc":1,
            "page":1,
            "product":product,
            "qid":1,
            "search_mode":search_mode	
        }

    def getQID(self, query):
        # query_links = []
        missing = True
        
        page_number = 0
        page_size = 50
        total_pages = 0

        # Run the query
        res = self.sess.get(self.search_url,params=self.search_get)
        self.sess.headers["Cookies"] = res.headers["set-cookie"]
        self.search_post["value(input1)"] = query
        self.sess.headers["Referer"] = 'http://apps.webofknowledge.com/WOS_GeneralSearch_input.do?product=WOS&search_mode=GeneralSearch&SID=W2uqUBWd5itguiU1tRO&preferencesSaved='
        
        results = self.sess.post(self.search_post_url,data=self.search_post)
        
        if results.status_code != 200:
            print "[ERROR] Failed query! Debug me (code: %s)" % (results.status_code)
            print "[DEBUG] search_post:"
            print self.search_post
            print self.sess.headers
            print results.url
            return

        root = html.fromstring(results.content)
        try:
            link = root.xpath('//div[@class="search-results"]//div[@class="search-results-content"]//a[@class="smallV110"]//@href')[1]
        except:
            print results.content
            print "[ERROR]: Could not get search results"

        # FIXME: find if no results returned
        # qid + JSESSIONID defines the result of the query. SOAP sucks
        self.paper_params["qid"] = dict(urlparse.parse_qsl(urlparse.urlsplit(link).query))["qid"]
        print "[DEBUG] QID: %s" % (self.paper_params["qid"])

    def nextResult(self):
        paper_result = self.sess.get(self.paper_url, params=self.paper_params)
        # FIXME: check problems (status code etc)
        root = html.fromstring(paper_result.content)
        links = root.xpath('//a[@class="paginationNext"]/@href')
        current_doc = self.paper_params["doc"]
        if len(links) > 0:
            link = links[0]
            getNext = True
            newparams = dict(urlparse.parse_qsl(urlparse.urlsplit(link).query))
            # Update for next search
            self.paper_params["doc"] = newparams["doc"]
            self.paper_params["page"] = newparams["page"]
        else:
            print "[WARNING] Cannot get next page. Assuming results are finished"
            getNext = False

        # Tell WOS we want to save "other" file type
        self.sess.post(self.save_config_url,
                       params=self.save_config_params,
                       data=self.save_config_post)
        # Tell WOS we wante the bibtex of the currently opened page
        self.save_bib_post["qid"] = self.paper_params["qid"]
        self.save_bib_post["mark_from"] = current_doc
        self.save_bib_post["mark_to"] = current_doc
        self.save_bib_post["rurl"] = paper_result.url

        bib = self.sess.post(self.save_bib_url,
                             params=self.save_bib_params,
                             data=self.save_bib_post)
        print bib.history
        
        # Extract this paper data
        newpaper = Paper()
        newpaper.search_on="WOS"

        newpaper.title = "".join(root.xpath('//div[@class="title"]/value//text()')).strip()
        newpaper.journal = root.xpath('//p[@class="sourceTitle"]/value/text()')[0]
        newpaper.authors = root.xpath('//div[@class="block-record-info"][1]/p[@class="FR_field"]/a/text()')
        newpaper.year = root.xpath('//div[@class="block-record-info block-record-info-source"]/p[@class="FR_field"]/value//text()')[0]
        newpaper.abstract = " ".join(root.xpath('//div[@class="block-record-info"][2]//p[@class="FR_field"]//text()'))
        newpaper.keywords["author"] = root.xpath('//div[@class="block-record-info"][3]//p[@class="FR_field"]/a/text()')
        newpaper.keywords["plus"] = root.xpath('//div[@class="block-record-info"][4]//p[@class="FR_field"]/a/text()')
        
        startpage, endpage = 0, 0
        for num in root.xpath('//div[@class="block-record-info-source-values"]//value/text()'):
            if "-" in num:
                pages = num.split("-")
                startpage = pages[0]
                endpage = pages[1]
        

        newpaper.pagespan = (startpage, endpage)

        return (getNext, newpaper)

    def StrToNum(self, string):
        return int(string.replace(",",""))
        
    def search(self, query):
        self.getQID(query)
        paperResults = []

        while True:
            tmpPaper = self.nextResult()
            self.lastPaper = tmpPaper[1]
            paperResults.append(self.lastPaper)
            if not tmpPaper[0]:
                break

        return paperResults
