class Paper:
    def __init__(self,
                 title    = "",
                 journal  = "",
                 doi      = "",
                 wosid    = "",
                 issn     = "",
                 scopus_id= "",
                 pagespan = (),
                 volume   = 0,
                 issue    = 0,
                 date     = 0,
                 abstract = "",
                 search_on= "",
                 SJR      = 0,
                 citeScore= 0,
                 papertype="",#review etc
                 keywords = None,
                 authors  = None):
        self.title    = title
        self.journal  = journal
        self.doi      = doi
        self.wosid    = wosid
        self.scopus_id= scopus_id
        self.issn     = issn
        self.pagespan = pagespan
        self.volume   = volume
        self.issue    = issue
        self.date     = date
        self.SJR      = SJR
        self.citeScore= citeScore
        self.papertype=papertype
        self.search_on=search_on
        self.abstract = abstract
        if keywords:
            self.keywords = keywords
        else:
            self.keywords = {"author":"","plus":""}
            
        if not authors:
            self.authors = []
        else:
            self.authors = authors
