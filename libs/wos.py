from collections import OrderedDict
from suds.client import Client

AUTH_URL = 'http://search.webofknowledge.com/esti/wokmws/ws/WOKMWSAuthenticate?wsdl'
auth = Client(AUTH_URL)

URL = 'http://search.webofknowledge.com/esti/wokmws/ws/WokSearchLite?wsdl'
client = Client(URL, headers= { 'Cookie': 'SID="%s; prID=704c00e2-b7a6-4532-b9b5-cf1af65bf3af"' % SID})

SID = 'X1WOtf4Omhyz7pVGDvg'

# Ovviamente il client che esiste non mi prende il session id. 
COOKIES = {"SID":SID}
HEADERS = {'Cookie':'SID="%s"' % SID}

query = "Test"
offset = 1
count = 2

qparams = OrderedDict([('databaseId', 'WOS'),
                        ('userQuery', query),
                        ('queryLanguage', 'en')])

rparams = OrderedDict([('firstRecord', offset),
                        ('count', count),
                        ('sortField', OrderedDict([('name', 'RS'),
                                                            ('sort', 'D')]))])

from suds.sax.element import Element

cookies = ('Cookie', 'SID="%s"' % SID)

res = client.service.search(qparams, rparams)


